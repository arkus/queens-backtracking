const val size = 25

val rows = MutableList(size) { false }
val diagonalLeft = MutableList(2 * size - 1) { false }
val diagonalRight = MutableList(2 * size - 1) { false }
val solution = MutableList(size) { 0 }
var foundSolution = false

fun main(args: Array<String>) {
    test(0)
}

fun isPossible(row: Int, column: Int): Boolean {
    return !rows[row] && !diagonalLeft[row + column] && !diagonalRight[row - column + size - 1]
}

fun putQueen(row: Int, column: Int) {
    solution[column] = row
    rows[row] = true
    diagonalLeft[row + column ] = true
    diagonalRight[row - column + size - 1] = true
}

fun removeQueen(row: Int, column: Int) {
    rows[row] = false
    diagonalLeft[row + column] = false
    diagonalRight[row - column + size - 1] = false
}

fun test(column: Int) {
//    if (foundSolution) {
//        return
//    }
    for (row in 0 until size) {
        if (isPossible(row, column)) {
            putQueen(row, column)
            if (column == size - 1) {
                foundSolution = true
                println(solution)
//                return
            } else {
                test(column + 1)
            }
            removeQueen(row, column)
        }
    }
}